#!/usr/bin/python3
#serial-linker GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.


import subprocess
import glob
import os


#setup script location:
mainDir = os.path.dirname(os.path.realpath(__file__))


#load config
configFileName = mainDir + "/linker_settings.txt"
configFile = open(configFileName).readlines()


#read all USBs
def readKeys():
  returnData = {}
  for ttyUSB in glob.glob("/dev/ttyUSB*"):
    command = f"udevadm info -q path -n {ttyUSB}".split()
    out,error = subprocess.Popen(command, stdout=subprocess.PIPE,stderr=subprocess.STDOUT).communicate()
    KEY = str(out).split('tty')[0]
    KEY = KEY.split("/")[-2]
    KEY = KEY.split('-')[-1]
    KEY = KEY.split(':')[0] #bug fix for ubuntu
    returnData[KEY] = ttyUSB
  return returnData

def LINK(name, device):
  global WHERE
  fullPath = WHERE + "/" + name
  print("Link: " + fullPath + " to device: " + str(device))
  if(os.path.islink(fullPath)):
    os.remove(fullPath) #remove old data
  os.symlink(device, fullPath)


WHERE = ""
keysWeHave = readKeys()

for line in configFile:
  if line.startswith('#'):
    continue
  if line.startswith('linkDir'):
    WHERE = line.split('=')[-1].strip()
  if line.startswith('serial'):
    name = line.split(":")[1]
    key = line.split(":")[-1].strip()
    if key in keysWeHave:
      LINK(name,keysWeHave[key])
    else:
      print("Error finding: " + str(key))
      
print("Run: picocom <switchName>")
print("Exit with: 'Ctrl + a' then 'Ctrl + x'")
