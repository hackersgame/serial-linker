#!/bin/bash
#serial-linker GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

clear
echo 'TTY: Key';for ttyUSB in `ls /dev/ttyUSB*`
do
  echo -n "$ttyUSB: "
  udevadm info -q path -n $ttyUSB | cut -d 't' -f1 | rev | cut -d '/' -f2 | rev | cut -d '-' -f2 | cut -d ':' -f1
done
