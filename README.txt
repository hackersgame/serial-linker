#serial-linker GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.


Creates readable and reliable names for serial devices. 


If you plan to use more than 4 serial devices, You'll need to set a kernel boot parameter:
    On Ubuntu 18.04 server: 
      Edit /etc/default/grub and append " 8250.nr_uarts=8" to the end of the line "GRUB_CMDLINE_LINUX_DEFAULT="
      Example: GRUB_CMDLINE_LINUX_DEFAULT="quiet 8250.nr_uarts=32"
      Run: update-grub
      Reboot

    On OpenSuSE TW:
      You should be good to go as: CONFIG_SERIAL_8250_NR_UARTS and CONFIG_SERIAL_8250_RUNTIME_UARTS are set to 32 in the Kernel.
      

    On some Distros you can try: 
      nr_uarts=32 (if serial support is built into the kernel)
      
    If setting nr_uarts and 8250.nr_uarts=32 did not work, it is likely the Kernel option CONFIG_SERIAL_8250_NR_UARTS that is the issue. (You'll need a new kernel/Distro) 


Run ./list_keys.sh to show your plugged in USB serial devices
The KEY will be unique to the USB port and should look like:
  X.X.X or X.X.X.X
  the last two positions should count up in a logical way:
  Port1 = 1.1
  Port2 = 1.2
  port3 = 1.3
  port4 = 1.4
  
  port5 = 2.1
  port6 = 2.2
  port7 = 2.3
  port8 = 2.4 ...
  
  
Once you know which devices are plugged into which portkey ;)
Edit linkder_settings.txt:
  linkDir is where your readable serial devices will end up.
  Add "serial:<readable name>:<KEY>" for each serial device
  
At this point, you can run ./linker.py and it should link things.


Or, finish the install with:
  sudo ln -s /path/to/linker.py /usr/bin/linker
  Then edit ~/.bashrc and add "linker" to the end #echo "linker" >> ~/.bashrc
Now the links will update every login.
